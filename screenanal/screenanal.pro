#-------------------------------------------------
#
# Project created by QtCreator 2018-01-29T16:01:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = screenanal
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    progressdialog.cpp

HEADERS  += mainwindow.h \
    controller.h \
    progressdialog.h



FORMS    += mainwindow.ui \
    progressdialog.ui

Win32
{
INCLUDEPATH  +=  $$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/include

LIBS  +=  -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/avcodec.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/avdevice.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/avfilter.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/avformat.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/avuti.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/postproc.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/swresample.lib \
          -L$$PWD/../../3rdParty/ffmpeg-20180127-a026a3e-win64-dev/lib/swscale.lib
}

DISTFILES +=

#RESOURCES += \
#    image.qrc
#RC_FILE = myapp.rc

RESOURCES += \
    m3ico.qrc
