#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <control.h>
#include <controller.h>
#include <QDir>
#include <QtGui>
#include <QIcon>
#include <QAbstractItemView>
#include <QCheckBox>
#include <QMessageBox>
#include <QColor>
#include "controller.h"
#include "progressdialog.h"

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui -> setupUi(this);
    MainWindow::buttonevent();
    this -> setWindowTitle("视频抽帧");
    ui -> listWidget -> setSelectionMode(QAbstractItemView::ExtendedSelection);
    setWindowIcon(QIcon(":/ico/ico/SmartMapping.ico"));
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::buttonevent()   //初始化ui
{
    connect(ui->inButton,  &QPushButton::clicked, this, &MainWindow::newInopenfile);
    connect(ui->tiaoshi,   &QPushButton::clicked, this, &MainWindow::opendia);
    connect(ui->outButton, &QPushButton::clicked, this, &MainWindow::outopenfile);
    connect(ui->chButton,  &QPushButton::clicked, this, &MainWindow::TFMimage2);
    connect(ui->pushButton,&QPushButton::clicked, this, &MainWindow::listWdelete);
    ui->pushButton->setEnabled(false);
    ui-> chButton ->setEnabled(false);
}
void MainWindow::opendia()
{
        //progressDiaLog *proDiaLog = new progressDiaLog;
        proDiaLog->show();
}
void MainWindow::listWdelete()   //删除
{
    QList <QListWidgetItem*> items ;             //注意items是个Qlist其中的元素是QListWidgetItem
    items = ui->listWidget->selectedItems();     //选中的数量
    if(items.size()==0)
    {
        ui->listWidget->setCurrentRow(0);
    }
    else
    {
        for(int i =0; i<items.size(); i++)        //遍历所算的ITEM
        {
            QListWidgetItem * sel = items[i];
            int r = ui->listWidget->row(sel);
            delete  ui->listWidget->takeItem(r);
        }
    }
    if(ui->listWidget->count() == 0)
    {
        ui->pushButton-> setEnabled(false);
        ui ->chButton -> setEnabled(false);
    }
}
void MainWindow::TFMimage2()      //转换格式
{
    int listsize = ui->listWidget->count();
    if( listsize == 0)
    {
        return;
    }
    else
    {
        for(int i =0; i<listsize; i++)   //遍历所算的item
        {
            QString inFileName = ui->listWidget->item(i)->text();
            qDebug()<<"inFileName:"<<inFileName;
            QString newfile = ui->lineEdit_2->text();
            qDebug()<<"newfile:"<<newfile;
            QString outFileName = ui->lineEdit_2->text();
            qDebug()<<"outFileName:"<<outFileName;
            QString box = ui->comboBox->currentText();
            qDebug()<<"box:"<<box;
            QStringList tmpList;
            int num = i;
            QString strNumber = QString::number(num,10);
            tmpList << strNumber <<inFileName << newfile << outFileName << box;
            listVector.push_back(tmpList);
        }
        MainWindow::dialogBar();
        Controller *my_thread = new Controller(listVector);
        my_thread -> startWork();
        proDiaLog -> show();
        QObject::connect(my_thread, &Controller::stringint, this, &MainWindow::change_font_state);
        QObject::connect(my_thread, &Controller::finished,  my_thread, &Controller::deleteLater);
        ui -> chButton-> setEnabled(false);
        listVector.clear();
        number = 0;
    }
}
void MainWindow::dialogBar() //初始化进度条
{
    int listsize = ui->listWidget->count();
    int sum = 0;
    if( listsize == 0)
    {
        return;
    }
    else
    {
        for(int i =0; i<listsize; i++)   //遍历所算的ITEM
        {
            QString inFileName = ui->listWidget->item(i)->text();
            QFileInfo fileInfo(inFileName);
            if(fileInfo.suffix() == "complete")
            {
                continue;
            }
            else
            {
              sum++;
            }
        }
    }
    qDebug() << sum;
    proDiaLog->progress(sum);
}

void MainWindow::change_font_state(QString i)     //更改界面状态
{
    int j = i.toInt();
    number++;
    QString inFileName = ui -> listWidget -> item(j)->text();
    QString outStr = inFileName + ".complete";
    ui -> listWidget-> item(j) -> setText (outStr);
    ui -> listWidget-> item(j) -> setForeground (QBrush(QColor( 0,196, 0))); 
    proDiaLog ->progressBar(number);
}

void MainWindow::newInopenfile() //视频输入
{
    QFileDialog *imgdial=new QFileDialog(this);    //打开一个窗口
    imgdial -> setWindowTitle(tr("openfile"));
    imgdial -> setViewMode(QFileDialog::Detail);
    QStringList filters;
    filters << QString("Image files (*.jpg *.wmv *.asf *.asx"
                       " *.Real *.Player *.mpg *.mpeg "
                       "*.mpe *.3gp *.mov *.mp4 *.m4v "
                       "*.mkv *.flv *.vob *.avi)");
    imgdial -> setNameFilters(filters);
    imgdial -> setFileMode(QFileDialog::ExistingFiles);
    QFileDialog::Options options;
    options = QFileDialog::DontUseNativeDialog;
    imgdial -> setOptions(options);
    if (imgdial->exec() == QDialog::Accepted)
    {
        strPathList = imgdial -> selectedFiles();
    }
    else
    {
       return;
    }
    if(strPathList.count() != 0) //按扭抬起
    {
         ui -> pushButton->setEnabled(true);
         ui -> chButton -> setEnabled(true);
         QFileInfo fileInfo(strPathList.at(0));
         ui -> lineEdit_2->setText (fileInfo.path());
         ui -> listWidget->addItems(strPathList);
         strPathList.clear();    //将新打开的数据传给listwidget后清除
    }
}
void MainWindow::outopenfile()   //输出路径
{
    QString file_path = QFileDialog::getExistingDirectory
            (this,"输出","E:\\wanshengzhe\\image");
    if(file_path.isEmpty())
    {
        return;
    }
    else
    {
        ui -> lineEdit_2 -> setText(file_path);
    }
}
