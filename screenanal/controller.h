#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <QObject>
#include <QThread>
#include <QDebug>
#include <QProcess>
#include <QFileDialog>
#include <QDebug>
#include <QDir>
#include <QtGui>
#include <QIcon>
#include <QVector>
#include "mainwindow.h"
#include "QApplication"

class MyWorker: public QObject
{
    Q_OBJECT
public:
    MyWorker(QObject* parent = nullptr)
    {

    }
    QVector<QStringList>workerlist;
    void Init(QVector<QStringList>list)
    {
        int number = list.size();
        if(workerlist.size() != 0)
        {
            workerlist.clear();
        }
        for(int i = 0; i < number; i++)
        {
            workerlist.push_back(list.at(i));
        }
    }
    ~MyWorker()
    {
    }
public slots:
    void doWork()
    {
        int number = workerlist.size();
        for(int i =0; i < number ; i++)
        {
            QStringList doWorkList = workerlist.at(i);
            QString inFileName  = doWorkList.at(1);
            QString newfile     = doWorkList.at(2);
            QString outFileName =doWorkList.at(3);
            QString box = doWorkList.at(4);
            QFileInfo fileInfo(inFileName);
            if(fileInfo.suffix() == "complete")
            {
                continue;
            }
            else
            {
                QDir dir;
                dir.cd(newfile);                     //进入某文件夹
                dir.mkdir(fileInfo.baseName());      //创建文件夹
                outFileName += "/" + fileInfo.baseName()
                        + "/" + fileInfo.baseName() + "%05d.jpg";
                QString exe_path=QApplication::applicationFilePath();
                QFileInfo temp(exe_path);
                QString program = temp.absolutePath()+"/ffmpeg-20180127-a026a3e-win64-shared/bin/ffmpeg.exe";
                qDebug()<<"program:"<<program;
                QStringList arguments;
                arguments <<"-i"<<inFileName<<"-r"<<box<<"-q:v"<<"2"<<"-f";
                arguments <<"image2"<<outFileName;
//                qDebug()<<arguments;
                QProcess *process = new QProcess;
                process -> start(program,arguments);    //运行cmd命令
                process -> waitForBytesWritten();
//                process -> waitForStarted(-1);
                process -> waitForFinished(-1);
//                delete process;
                emit finnumber(doWorkList.at(0));
            }
        }
        emit finished();
    }
signals:
    void finished();
    void finnumber(QString i);
};

class Controller : public QObject
{
    Q_OBJECT
    QThread workerThread;
    QVector<QStringList>listVector;
public:
    Controller(QVector<QStringList>list,QObject *parent= nullptr)
    {
        if(listVector.size() != 0)
        {
            listVector.clear();
        }
        int number = list.size();
        for(int i = 0; i < number; i++)
        {
            listVector.push_back(list.at(i));
        }
    }
    Controller(QObject *parent= nullptr)
    {

    }
    void startWork()
    {
        QThread * workerThread = new QThread;
        MyWorker *worker = new MyWorker;
        worker -> Init(listVector);
        worker -> moveToThread(workerThread);
        QObject::connect(workerThread, &QThread::started,  worker, &MyWorker::doWork);
        QObject::connect(worker, &MyWorker::finished,   workerThread, &QThread::quit);
        QObject::connect(worker, &MyWorker::finished,   [](){qDebug() << QThread::currentThreadId()<<" quit......";});
        QObject::connect(worker, &MyWorker::finished,   worker, &MyWorker::deleteLater);
        QObject::connect(worker, &MyWorker::finnumber,  this, &Controller::strint);
        QObject::connect(worker, &MyWorker::finished,   this, &Controller::finished);
        QObject::connect(workerThread, &QThread::finished, workerThread, &QThread::deleteLater);
        workerThread->start();         //启动线程
    }
    void findChild()
    {
    }
    ~Controller()
    {
    }
public slots:
    void strint(QString i)
    {
        //qDebug()<< i;
        emit stringint(i);
    }

signals:
    void operate(const int);//发送信号触发线程
    void stringint(QString i);
    void finished();
private:
    int number;
};
#endif // CONTROLLER_H
