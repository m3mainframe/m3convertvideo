#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QAction>
#include <QWidget>
#include <QString>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavdevice/avdevice.h>
#include <libavformat/version.h>
#include <libavutil/time.h>
#include <libavutil/mathematics.h>
#include <QDebug>
#include <QProcess>
#include <QMessageBox>
#include <QStringList>
#include <QDialog>
#include "progressdialog.h"


namespace Ui
{
    class MainWindow;
}
//extern Dialog *dia;
class MainWindow : public QMainWindow// ,public QObject
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void outopenfile();
    void TFMimage();
    void TFMimage2();
    void listWdelete();
    void buttonevent();
    void makefile();
    void newInopenfile();
    void opendia();
    void change_font_state(QString i);
    void dialogBar();
    QStringList outList;
    QStringList strPathList;
    QVector<QStringList>listVector;
    int number = 0;
    progressDiaLog *proDiaLog = new progressDiaLog;
    //ProgDialog *dia = new ProgDialog;
private:
    Ui::MainWindow *ui;
//signals:
//    void resultReady(const int result);
};

#endif // MAINWINDOW_H
