#include "dialog.h"
#include "ui_dialog.h"
ProgDialog::ProgDialog(QWidget *parent) :QDialog(parent),ui(new Ui::ProgDialog)
{
    ui->setupUi(this);
    ui->progressBar->setValue(0);
}

void ProgDialog::progress(int sum)
{
    ui->progressBar->setRange(0, sum);
}
void ProgDialog::progressBar(int number)
{
    ui->progressBar->setValue(number);
}
ProgDialog::~ProgDialog()
{
    delete ui;
}
