#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>

namespace Ui
{
    class progressDiaLog;
}

class progressDiaLog : public QDialog
{
    Q_OBJECT

public:
    void progressBar(int number);
    void progress(int sum);

public:
    explicit progressDiaLog(QWidget *parent = 0);
    ~progressDiaLog();

private:
    Ui::progressDiaLog *ui;
};

#endif // PROGRESSDIALOG_H
