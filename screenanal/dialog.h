#ifndef DIALOG_H
#define DIALOG_H
#include "controller.h"
#include <QDialog>
namespace Ui
{
    class ProgDialog;
}
class ProgDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProgDialog(QWidget *parent = 0);
    void progress(int sum);
    void progressBar(int number);
    ~ProgDialog();

private:
    Ui::ProgDialog *ui;
};

#endif // DIALOG_H
